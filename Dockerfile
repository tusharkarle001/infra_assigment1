FROM node:14.19-alpine3.14
WORKDIR /user/src/app
COPY package.json ./
run npm install
COPY . .
EXPOSE 51005
CMD [ "npm", "run","dev"]